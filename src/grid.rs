use std::vec::Vec;
use ggez::{Context, GameResult};
use ggez::graphics::{
    self,
    Drawable,
    DrawParam,
    Rect,
    BlendMode,
};
use ggez::graphics::spritebatch::SpriteBatch;
use glam::{Vec2, UVec2};

use crate::assets::Assets;
use crate::constants::{
    BRICK_SIZE,
    GRID_X,
    GRID_Y,
    GRID_WIDTH,
    GRID_HEIGHT,
};
use crate::piece::{Bricks, Piece};

pub struct Grid {
    bricks: Bricks,
    spritebatch: SpriteBatch,
}

impl Grid {
    pub fn new(assets: &Assets) -> Grid {
        let mut bricks = Vec::<Vec<u32>>::new();
        for _ in 0..GRID_HEIGHT as u32 {
            let mut row = Vec::<u32>::new();
            for _ in 0..GRID_WIDTH as u32 {
                row.push(0);
            }
            bricks.push(row);
        }
        Grid {
            bricks: bricks,
            spritebatch: SpriteBatch::new(assets.blocks.clone()),
        }
    }

    pub fn check_collision(&self, pos: UVec2, piece: &Piece) -> bool {
        if (pos.x + piece.width() as u32) as f32 > GRID_WIDTH {
            return true;
        }
        if (pos.y + piece.height() as u32) as f32 > GRID_HEIGHT {
            return true;
        }
        for (y, row) in piece.bricks.clone().into_iter().enumerate() {
            for (x, brick) in row.into_iter().enumerate() {
                let x = x + pos.x as usize;
                let y = y + pos.y as usize;
                if brick > 0 && self.bricks[y][x] > 0 {
                    return true;
                }
            }
        }
        false
    }

    pub fn apply_piece(&mut self, piece: &Piece, assets: &Assets) -> u32 {
        for (y, row) in piece.bricks.clone().into_iter().enumerate() {
            for (x, brick) in row.into_iter().enumerate() {
                if brick > 0 {
                    let grid_pos = piece.get_grid_pos();
                    let x = grid_pos.x as usize + x;
                    let y = grid_pos.y as usize + y;
                    self.bricks[y][x] = brick * piece.color;
                }
            }
        }
        self.bricks = self.bricks.clone().into_iter().filter(|row| {
            let mut full = true;
            for brick in row.into_iter() {
                if *brick == 0 {
                    full = false;
                    break;
                }
            }
            !full
        }).collect();
        let removed_rows = GRID_HEIGHT as usize - self.bricks.len();
        let score = removed_rows as u32 * 100;
        if removed_rows > 0 {
            for _ in 0..removed_rows {
                let mut row = Vec::<u32>::new();
                for _ in 0..GRID_WIDTH as u32 {
                    row.push(0);
                }
                self.bricks.insert(0, row);
            }
        }
        self.spritebatch = SpriteBatch::new(assets.blocks.clone());
        for (y, row) in self.bricks.clone().into_iter().enumerate() {
            for (x, brick) in row.into_iter().enumerate() {
                if brick > 0 {
                    let x = x as f32;
                    let y = y as f32;
                    self.spritebatch.add(
                        DrawParam::new()
                            .src(Rect::new(
                                (brick - 1) as f32 / 6.0,
                                0.0,
                                1.0 / 6.0,
                                1.0
                            ))
                            .dest(Vec2::new(
                                x * BRICK_SIZE,
                                y * BRICK_SIZE
                            ))
                    );
                }
            }
        }
        score
    }
}


impl Drawable for Grid {
    fn draw(&self, ctx: &mut Context, param: DrawParam) -> GameResult {
        let param = param.clone()
            .dest(Vec2::new(
                GRID_X,
                GRID_Y
            ));
        graphics::draw(ctx, &self.spritebatch, param)?;
        Ok(())
    }

    fn dimensions(&self, _ctx: &mut Context) -> Option<Rect> {
        Some(Rect::new(
            0.0,
            0.0,
            BRICK_SIZE * GRID_WIDTH,
            BRICK_SIZE * GRID_HEIGHT
        ))
    }

    fn set_blend_mode(&mut self, mode: Option<BlendMode>) {
        self.spritebatch.set_blend_mode(mode);
    }

    fn blend_mode(&self) -> Option<BlendMode> {
        self.spritebatch.blend_mode()
    }
}