use std::vec::Vec;
use ggez::{
    Context,
    GameResult,
    GameError,
};
use ggez::graphics::{
    self,
    Drawable,
    DrawParam,
    Rect,
    BlendMode,
};
use ggez::graphics::spritebatch::SpriteBatch;
use glam::{Vec2, UVec2};
use rand::Rng;
use rand::seq::SliceRandom;

use crate::assets::Assets;
use crate::constants::{
    BRICK_SIZE,
    GRID_X,
    GRID_Y
};

pub type Bricks = Vec<Vec<u32>>;

pub struct Piece {
    pub pos: Vec2,
    pub bricks: Bricks,
    pub color: u32,
    spritebatch: SpriteBatch
}

impl Piece {
    pub fn new(pos: Vec2, color: u32, template: &Bricks, assets: &Assets) -> GameResult<Piece> {
        let mut spritebatch = SpriteBatch::new(assets.blocks.clone());
        let mut bricks = Vec::<Vec<u32>>::new();
        for (y, template_row) in template.into_iter().enumerate() {
            let mut row = Vec::<u32>::new();
            for (x, template_brick) in template_row.into_iter().enumerate() {
                row.push(*template_brick);
                if *template_brick > 0 {
                    let x = x as f32;
                    let y = y as f32;
                    spritebatch.add(
                        DrawParam::new()
                            .src(Rect::new(
                                (color - 1) as f32 / 6.0,
                                0.0,
                                1.0 / 6.0,
                                1.0
                            ))
                            .dest(Vec2::new(
                                x * BRICK_SIZE,
                                y * BRICK_SIZE
                            ))
                    );
                }
            }
            bricks.push(row);
        }
        Ok(Piece {
            pos: pos,
            bricks: bricks,
            color: color,
            spritebatch: spritebatch,
        })
    }

    pub fn generate(assets: &Assets, templates: &Vec<Bricks>) -> GameResult<Piece> {
        let mut rng = rand::thread_rng();
        let color = rng.gen_range(1..7);
        match templates.choose(&mut rng) {
            Some(template) => {
                Piece::new(Vec2::new(0.0, 0.0), color, template, assets)
            },
            None => Err(GameError::CustomError("Could not generate brick pattern".to_string()))
        }
    }

    pub fn width(&self) -> usize {
        self.bricks[0].len()
    }

    pub fn height(&self) -> usize {
        self.bricks.len()
    }

    pub fn set_grid_pos(&mut self, pos: UVec2) {
        let x = pos.x as f32;
        let y = pos.y as f32;
        self.pos = Vec2::new(
            GRID_X + (x * BRICK_SIZE),
            GRID_Y + (y * BRICK_SIZE)
        );
    }

    pub fn get_grid_pos(&self) -> UVec2 {
        UVec2::new(
            ((self.pos.x - GRID_X) / BRICK_SIZE).round() as u32,
            ((self.pos.y - GRID_Y) / BRICK_SIZE).round() as u32
        )
    }

    pub fn rotated(&self, assets: &Assets) -> GameResult<Piece> {
        let mut bricks = Vec::<Vec<u32>>::new();
        for x in 0..self.width() {
            let mut row = Vec::<u32>::new();
            for y in (0..self.height()).rev() {
                row.push(self.bricks[y][x]);
            }
            bricks.push(row);
        }
        Piece::new(self.pos, self.color, &bricks, assets)
    }
}

impl Drawable for Piece {
    fn draw(&self, ctx: &mut Context, param: DrawParam) -> GameResult {
        graphics::draw(ctx, &self.spritebatch, param.clone().dest(self.pos))?;
        Ok(())
    }

    fn dimensions(&self, _ctx: &mut Context) -> Option<Rect> {
        Some(Rect::new(
            self.pos.x,
            self.pos.y,
            BRICK_SIZE * self.width() as f32,
            BRICK_SIZE * self.height() as f32
        ))
    }

    fn set_blend_mode(&mut self, mode: Option<BlendMode>) {
        self.spritebatch.set_blend_mode(mode);
    }

    fn blend_mode(&self) -> Option<BlendMode> {
        self.spritebatch.blend_mode()
    }
}