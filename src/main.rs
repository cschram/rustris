#![windows_subsystem = "windows"]

use std::mem;
use std::vec::Vec;
use ggez::audio::SoundSource;
use ggez::{Context, GameResult};
use ggez::event;
use ggez::timer;
use ggez::conf::{WindowSetup, NumSamples, WindowMode, FullscreenType};
use ggez::graphics::{self, Color, DrawParam};
use glam::{Vec2, UVec2};

pub mod assets;
pub mod background;
pub mod constants;
pub mod grid;
pub mod piece;

use crate::assets::Assets;
use crate::background::Background;
use crate::constants::{
    TARGET_FPS,
    SCREEN_WIDTH,
    SCREEN_HEIGHT,
    GRID_WIDTH,
    GRID_HEIGHT,
    BRICK_SIZE,
    FALL_DELAY_START,
    FALL_DELAY_INCREMENT,
    FALL_DECREASE_DELAY,
    INPUT_DELAY,
};
use crate::grid::Grid;
use crate::piece::{Bricks, Piece};

struct Input {
    left: bool,
    right: bool,
    down: bool,
    space: bool,
}

struct GameState {
    assets: Assets,
    templates: Vec<Bricks>,
    background: Background,
    input: Input,
    score: u32,
    grid: Grid,
    current_piece: Piece,
    current_piece_pos: UVec2,
    next_piece: Piece,
    input_timer: f32,
    fall_timer: f32,
    fall_delay: f32,
    fall_decrease_timer: f32,
}

impl GameState {
    fn new(ctx: &mut Context) -> GameResult<GameState> {
        let mut assets = Assets::new(ctx)?;
        let templates = vec![
            vec![
                vec![1],
                vec![1],
                vec![1],
                vec![1],
            ],
            vec![
                vec![1,  1],
                vec![1,  1],
            ],
            vec![
                vec![0, 1, 0],
                vec![1, 1, 1],
            ],
            vec![
                vec![1, 1],
                vec![0, 1],
                vec![0, 1],
            ],
            vec![
                vec![1, 1],
                vec![1, 0],
                vec![1, 0],
            ],
            vec![
                vec![0, 1],
                vec![1, 1],
                vec![1, 0],
            ],
            vec![
                vec![1, 0],
                vec![1, 1],
                vec![0, 1],
            ],
        ];
        let mut current_piece = Piece::generate(&assets, &templates)?;
        let current_piece_pos = UVec2::new(4, 0);
        current_piece.set_grid_pos(current_piece_pos);
        let mut next_piece = Piece::generate(&assets, &templates)?;
        next_piece.pos = Vec2::new(
            (18.0 - (next_piece.width() as f32 / 2.0)) * BRICK_SIZE,
            (23.0 - (next_piece.height() as f32 / 2.0)) * BRICK_SIZE
        );
        let background = Background::new(ctx, &assets)?;
        let grid = Grid::new(&assets);
        assets.theme.set_repeat(true);
        assets.theme.play(ctx)?;
        Ok(GameState {
            assets: assets,
            templates: templates,
            background: background,
            input: Input {
                left: false,
                right: false,
                down: false,
                space: false,
            },
            score: 0,
            grid: grid,
            current_piece: current_piece,
            current_piece_pos: current_piece_pos,
            next_piece: next_piece,
            input_timer: 0.0,
            fall_timer: FALL_DELAY_START,
            fall_delay: FALL_DELAY_START,
            fall_decrease_timer: FALL_DECREASE_DELAY,
        })
    }

    fn set_score(&mut self, score: u32) {
        self.score = score;
        self.background.set_score(score);
    }

    fn move_left(&mut self) {
        if self.current_piece_pos.x > 0 {
            let new_pos = self.current_piece_pos - UVec2::new(1, 0);
            if !self.grid.check_collision(new_pos, &self.current_piece) {
                self.current_piece.set_grid_pos(new_pos);
                self.current_piece_pos = new_pos;
            }
        }
    }

    fn move_right(&mut self) {
        if (self.current_piece_pos.x  + self.current_piece.width() as u32) < GRID_WIDTH as u32 {
            let new_pos = self.current_piece_pos + UVec2::new(1, 0);
            if !self.grid.check_collision(new_pos, &self.current_piece) {
                self.current_piece.set_grid_pos(new_pos);
                self.current_piece_pos = new_pos;
            }
        }
    }

    fn move_down(&mut self) -> GameResult {
        if (self.current_piece_pos.y + self.current_piece.height() as u32) < GRID_HEIGHT as u32 {
            let new_pos = self.current_piece_pos + UVec2::new(0, 1);
            if self.grid.check_collision(new_pos, &self.current_piece) {
                self.apply_current_piece()?;
            } else {
                self.current_piece.set_grid_pos(new_pos);
                self.current_piece_pos = new_pos;
            }
        } else {
            self.apply_current_piece()?;
        }
        Ok(())
    }

    fn rotate(&mut self) -> GameResult {
        let rotated_piece = self.current_piece.rotated(&self.assets)?;
        if !self.grid.check_collision(self.current_piece_pos, &rotated_piece) {
            self.current_piece = rotated_piece;
        }
        Ok(())
    }

    fn apply_current_piece(&mut self) -> GameResult {
        let score = self.grid.apply_piece(&self.current_piece, &self.assets);
        if score > 0 {
            self.set_score(self.score + score);
        }
        let mut next_piece = Piece::generate(&self.assets, &self.templates)?;
        next_piece.pos = Vec2::new(
            (18.0 - (next_piece.width() as f32 / 2.0)) * BRICK_SIZE,
            (23.0 - (next_piece.height() as f32 / 2.0)) * BRICK_SIZE
        );
        self.current_piece = mem::replace(&mut self.next_piece, next_piece);
        self.current_piece_pos = UVec2::new(4, 0);
        self.current_piece.set_grid_pos(self.current_piece_pos);
        Ok(())
    }
}

impl event::EventHandler<ggez::GameError> for GameState {
    fn update(&mut self, ctx: &mut Context) -> GameResult {
        let delta = timer::delta(ctx).as_secs_f32();
        if self.input_timer > 0.0 {
            self.input_timer -= delta;
        }
        if self.fall_timer > 0.0 {
            self.fall_timer -= delta;
        }
        if self.fall_decrease_timer > 0.0 {
            self.fall_decrease_timer -= delta;
        }
        while timer::check_update_time(ctx, TARGET_FPS) {
            if self.input_timer <= 0.0 {
                if self.input.left {
                    self.move_left();
                    self.input_timer = INPUT_DELAY;
                } else if self.input.right {
                    self.move_right();
                    self.input_timer = INPUT_DELAY;
                }
                if self.input.down {
                    self.move_down()?;
                    self.input_timer = INPUT_DELAY;
                }
                if self.input.space {
                    self.rotate()?;
                    self.input_timer = INPUT_DELAY;
                }
            }
            if self.fall_timer <= 0.0 {
                self.move_down()?;
                self.fall_timer = self.fall_delay;
            }
            if self.fall_decrease_timer <= 0.0 {
                self.fall_decrease_timer = FALL_DECREASE_DELAY;
                self.fall_delay -= FALL_DELAY_INCREMENT;
            }
        }
        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult {
        graphics::clear(ctx, Color::BLACK);
        graphics::draw(ctx, &self.background, DrawParam::new())?;
        graphics::draw(ctx, &self.grid, DrawParam::new())?;
        graphics::draw(ctx, &self.current_piece, DrawParam::new())?;
        graphics::draw(ctx, &self.next_piece, DrawParam::new())?;
        graphics::present(ctx)?;
        Ok(())
    }

    fn key_down_event(&mut self, ctx: &mut Context, keycode: event::KeyCode, _keymods: event::KeyMods, _repeat: bool) {
        match keycode {
            event::KeyCode::Left => {
                self.input.left = true;
            },
            event::KeyCode::Right => {
                self.input.right = true;
            },
            event::KeyCode::Down => {
                self.input.down = true;
            },
            event::KeyCode::Space => {
                self.input.space = true;
            },
            event::KeyCode::Escape => {
                event::quit(ctx);
            },
            _ => ()
        }
    }

    fn key_up_event(&mut self, _ctx: &mut Context, keycode: event::KeyCode, _keymods: event::KeyMods) {
        match keycode {
            event::KeyCode::Left => {
                self.input.left = false;
            },
            event::KeyCode::Right => {
                self.input.right = false;
            },
            event::KeyCode::Down => {
                self.input.down = false;
            },
            event::KeyCode::Space => {
                self.input.space = false;
            },
            _ => ()
        }
    }
}

fn main() -> GameResult {
    let cb = ggez::ContextBuilder::new("Rustris", "Corey Schram")
        .window_setup(WindowSetup {
            title: "Rustris".to_owned(),
            samples: NumSamples::One,
            vsync: true,
            icon: "".to_owned(),
            srgb: true
        })
        .window_mode(WindowMode {
            width: SCREEN_WIDTH,
            height: SCREEN_HEIGHT,
            maximized: false,
            fullscreen_type: FullscreenType::Windowed,
            borderless: false,
            min_width: 0.0,
            max_width: 0.0,
            min_height: 0.0,
            max_height: 0.0,
            resizable: false,
            visible: true,
            resize_on_scale_factor_change: false,
        });
    let (mut ctx, event_loop) = cb.build()?;
    let state = GameState::new(&mut ctx)?;
    event::run(ctx, event_loop, state)
}
