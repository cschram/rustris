use ggez::{Context, GameResult};
use ggez::graphics::{
    self,
    Rect,
    Mesh,
    Font,
    Text,
    TextFragment,
    Color,
    DrawMode,
    Drawable,
    DrawParam,
    BlendMode,
};
use ggez::graphics::spritebatch::SpriteBatch;
use glam::Vec2;

use crate::assets::Assets;
use crate::constants::{
    SCREEN_WIDTH,
    SCREEN_HEIGHT,
    BRICK_SIZE,
    GRID_WIDTH,
    GRID_HEIGHT
};

pub struct Background {
    bg: SpriteBatch,
    font: Font,
    grid_bg: Mesh,
    next_piece_bg: Mesh,
    score_label_text: Text,
    score_text: Text,
    next_label_text: Text,
}

impl Background {
    pub fn new(ctx: &mut Context, assets: &Assets) -> GameResult<Background> {
        let mut bg = SpriteBatch::new(assets.bg.clone());
        let tiles_x = (SCREEN_WIDTH / 32.0) as u32;
        let tiles_y = (SCREEN_HEIGHT / 32.0) as u32;
        for x in 0..tiles_x {
            for y in 0..tiles_y {
                bg.add(
                    DrawParam::new()
                        .dest(Vec2::new((x * 32) as f32, (y * 32) as f32))
                );
            }
        }
        let grid_bg = Mesh::new_rectangle(
            ctx,
            DrawMode::fill(),
            Rect::new(
                BRICK_SIZE * 2.0,
                0.0,
                GRID_WIDTH * BRICK_SIZE,
                GRID_HEIGHT * BRICK_SIZE
            ),
            Color::from_rgb(60, 60, 249)
        )?;
        let next_piece_bg = Mesh::new_rectangle(
            ctx,
            DrawMode::fill(),
            Rect::new(
                BRICK_SIZE * 14.0,
                BRICK_SIZE * (GRID_HEIGHT - 8.0),
                BRICK_SIZE * 8.0,
                BRICK_SIZE * 6.0
            ),
            Color::from_rgb(60, 60, 249)
        )?;
        let score_label_text = Text::new(
            TextFragment::new("SCORE")
                .font(assets.roboto_mono)
                .color(Color::WHITE)
        );
        let score_text = Text::new(
            TextFragment::new("0")
                .font(assets.roboto_mono)
                .color(Color::WHITE)
        );
        let next_label_text = Text::new(
            TextFragment::new("NEXT")
                .font(assets.roboto_mono)
                .color(Color::WHITE)
        );
        Ok(Background {
            bg: bg,
            font: assets.roboto_mono,
            grid_bg: grid_bg,
            next_piece_bg: next_piece_bg,
            score_label_text: score_label_text,
            score_text: score_text,
            next_label_text: next_label_text,
        })
    }

    pub fn set_score(&mut self, score: u32) {
        self.score_text = Text::new(
            TextFragment::new(score.to_string())
                .font(self.font)
                .color(Color::WHITE)
        );
    }
}

impl Drawable for Background {
    fn draw(&self, ctx: &mut Context, param: DrawParam) -> GameResult {
        graphics::draw(ctx, &self.bg, param)?;
        graphics::draw(ctx, &self.grid_bg, (Vec2::ZERO,))?;
        graphics::draw(ctx, &self.next_piece_bg, (Vec2::ZERO,))?;
        graphics::draw(ctx, &self.score_label_text, (Vec2::new(
            BRICK_SIZE * 14.0,
            BRICK_SIZE * 2.0
        ),))?;
        graphics::draw(ctx, &self.score_text, (Vec2::new(
            BRICK_SIZE * 14.0,
            BRICK_SIZE * 4.0
        ),))?;
        graphics::draw(ctx, &self.next_label_text, (Vec2::new(
            BRICK_SIZE * 14.0,
            BRICK_SIZE * 18.0
        ),))?;
        Ok(())
    }

    fn dimensions(&self, _ctx: &mut Context) -> Option<Rect> {
        Some(Rect::new(0.0, 0.0, SCREEN_WIDTH, SCREEN_HEIGHT))
    }

    fn set_blend_mode(&mut self, mode: Option<BlendMode>) {
        self.bg.set_blend_mode(mode);
        self.grid_bg.set_blend_mode(mode);
        self.next_piece_bg.set_blend_mode(mode);
        self.score_label_text.set_blend_mode(mode);
        self.score_text.set_blend_mode(mode);
        self.next_label_text.set_blend_mode(mode);
    }

    fn blend_mode(&self) -> Option<BlendMode> {
        self.bg.blend_mode()
    }
}