use ggez::{GameResult, Context};
use ggez::graphics::{Image, Font};
use ggez::audio::Source;

pub struct Assets {
    pub bg: Image,
    pub blocks: Image,
    pub roboto_mono: Font,
    pub theme: Source,
}

impl Assets {
    pub fn new(ctx: &mut Context) -> GameResult<Assets> {
        Ok(Assets {
            bg: Image::new(ctx, "/bg.png")?,
            blocks: Image::new(ctx, "/blocks.png")?,
            roboto_mono: Font::new(ctx, "/roboto_mono_regular.ttf")?,
            theme: Source::new(ctx, "/theme.mp3")?,
        })
    }
}
